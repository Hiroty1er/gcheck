const get = require('./getJSON.js');

// Permet d'initialiser la configuration du nom de domaine que l'on souhaite utiliser
// Exemple -> {"isRefer":"duniter.g1.1000i100.fr"}
// La configuration fera pointé la fonction "isRefer" vers le domaine de "duniter.g1.1000i100.fr"

let globalConf;
function init(json={"isSentry":"duniter.g1.1000i100.fr"}){
    globalConf = json;
}
module.exports.init = init;

async function isSentry (wallet) {
    const apiResult = await get.json('https://'+globalConf.isSentry+'/wot/requirements/'+wallet);

    // Si l'identité récupéré correspond parfaitement à celle recherché
    if (apiResult.identities && (apiResult.identities[0].uid == wallet || apiResult.identities[0].pubkey == wallet))
    {
      if (apiResult.identities[0].isSentry == true) 
      { return true; }
      else { return false; }
    }
    else { return "Je n'arrive pas à identifier clairement le propriétaire, pouvez vous être plus précis ?"; }
}
module.exports.isSentry = isSentry;