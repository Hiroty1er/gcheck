'use strict';
const appisSentry = require('./isSentry.js');
const appisMember = require('./isMember.js');
const Hapi = require('@hapi/hapi');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({ //Page d'accueil de l'API
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return `<p> Bienvenue sur l'API g1lien<br/><br/>
                        Les fonctions disponible sont actuellement les suivantes:<br/><br/>
                        - isMember:<wallet> indique le statut de membre ou non de <wallet><br/>
                            .../isMember/1000i100<br/>
                            .../isMember/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT<br/>
                            vous envoie le résultat:<br/> 
                            >true<br/><br/>

                        - isSentry:<wallet> indique le statut de membre référent / sentinel du <wallet><br/>
                            .../isSentry/Hiroty<br/>
                            .../isSentry/D6Pm9VsPTLqYMwUtcXxXBqdGP9pMXMkd76C1xZXsF3yg<br/>
                            vous envoie le résultat:<br/>
                            >false<br/>
                    </p>`;
        }
    });

    server.route({
        method: 'GET',
        path: '/isMember/{wallet}', //id = pubkey || uid
        handler: (request, h) => {
            appisMember.init();
            return appisMember.isMember(request.params.wallet);
        }
    });

    server.route({
        method: 'GET',
        path: '/isSentry/{wallet}', //id = pubkey || uid
        handler: (request, h) => {
            appisSentry.init();
            return appisSentry.isSentry(request.params.wallet);
        }
    })

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();