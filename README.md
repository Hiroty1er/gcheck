## API GCheck
Gcheck est une API qui simplifie les résultats de données de la block-chain renvoyé par BMA.

Elle comprends actuellement les deux fonction suivante:

`<wallet>`: correspond à une clée publique ou un UID d'un compte duniter.

`isMember:<wallet>` indique le statut de membre ou non de `<wallet>`
    .../isMember/1000i100
    .../isMember/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT
vous envoie le résultat: 
>\>true

`isSentry:<wallet>` indique le statut de membre référent / sentinel du `<wallet>`
    .../isSentry/Hiroty
    .../isSentry/D6Pm9VsPTLqYMwUtcXxXBqdGP9pMXMkd76C1xZXsF3yg
vous envoie le résultat:
>\>false

Il est possible de que vous recevez le message suivant:
>Je n'arrive pas à identifier clairement le propriétaire, pouvez vous être plus précis ?

Cela signifie que l'uid ou la clée publique ne correspond pas exactement à ce qu'y est trouvé dans la block-chain.

D'autre fonction seront à venir en correspondance avec les besoins de [G1lien](https://git.duniter.org/clients/g1lien).

## Installation
>\> npm install @hapi/hapi

## Execution du service
>\> node gCheck.js

## Accès extèrieur
>\> `<localhost>`:3000/`<fonction>`/`<wallet>`